<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('startingPage');
});
Route::get('/home', 'ClientController@index')->name('home')->middleware('auth');
Route::put('add_to_parking', 'CarController@add_to_parking')->name('add_to_parking')->middleware('auth');
Route::resource('client', 'ClientController')->middleware('auth');
Route::resource('car', 'CarController')->middleware('auth');

Auth::routes();

