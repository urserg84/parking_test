<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model
{

	public function get_clients_off_parking()
	{
		 return DB::table('clients')
	            ->join('cars', 'clients.id', '=', 'cars.client_id')
	            ->where('cars.on_parking', '=', '0')
	            ->get();
	}

	public function get_cars_off_parking()
	{
		 return DB::table('cars')
	            ->where('on_parking', '=', '0')
	            ->get();
	}

	public function store_car($brand, 
								$model, 
								$color, 
								$number, 
								$client_id, 
								$on_parking, 
								$created_at, 
								$updated_at)
	{
		DB::insert('insert into cars (brand, model, color, number, client_id, on_parking, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?)', [$brand,    
		                                                   $model, 
		                                                   $color, 
		                                                   $number, 
		                                                   $client_id, 
		                                                   $on_parking, 
		                                                   $created_at, 
		                                                   $updated_at]); 
	}

	public function update_car($brand, 
								$model, 
								$color, 
								$number,  
								$on_parking, 
								$created_at, 
								$updated_at,
								$id)
	{
		DB::update('update cars 
            SET brand = ?, 
            model = ?, 
            color = ?, 
            number = ?,
            on_parking = ?, 
            created_at = ?, 
            updated_at = ? 
            where car_id = ?', [$brand, 
								$model, 
								$color, 
								$number,  
								$on_parking, 
								$created_at, 
								$updated_at,
								$id]); 
	}

	public function car_destroy($id)
	{
		DB::update('update cars set on_parking = 0 where car_id = ?', [$id]);
	}

	public function add_to_parking($id)
	{
		DB::update('update cars set on_parking = 1 where car_id = ?', [$id]);
	}

	public function get_current_number($id)
	{
		return DB::select('select number from cars where car_id = ?', [$id])[0]->number;
	}

	public function get_carbrands()
	{
		 return DB::table('carbrands')->get();
	            
	}
}
