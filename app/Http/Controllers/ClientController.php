<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Car;
use App\Client;

class ClientController extends Controller
{

    public function __construct()
    {   
        $this->ClientModel = new Client();
        $this->CarModel = new Car();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = $this->ClientModel->get_cars();

        return view('home', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carbrands = $this->CarModel->get_carbrands();
        return view('create', compact('carbrands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'full_name' => 'required|min:3',
            'gender' => 'required',
            'phone' => 'required|unique:clients,phone',
            'address' => 'required',
            'brand' => 'required',
            'model' => 'required',
            'color' => 'required',
            'number' => 'required|unique:cars,number',
            'on_parking' => 'nullable',
        ]);

        $this->ClientModel->store_client(request('full_name'), request('gender'), request('phone'), request('address'), new \DateTime(), new \DateTime());

        $last_client_id = $this->ClientModel->get_last_client_id();

        $on_parking = request('on_parking');
        if (!request('on_parking')) {
            $on_parking = 0;
        }

        $this->ClientModel->store_car(request('brand'), request('model'), request('color'), request('number'), $last_client_id, $on_parking, new \DateTime(), new \DateTime());

        Session::flash('success', 'Ваша запись сохранена');

        return redirect()->route('client.edit', $last_client_id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = $this->ClientModel->edit_client($id);
        $carbrands = $this->CarModel->get_carbrands();

        return view('client.edit')
                ->with('client', $client)
                ->with('carbrands', $carbrands);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $current_phone = $this->ClientModel->get_current_phone($id);

        if (request('phone') == $current_phone) {
            request()->validate([
                'full_name' => 'required|min:3',
                'gender' => 'required',
                'phone' => 'required',
                'address' => 'required',
                ]);
        } else {
            request()->validate([
            'full_name' => 'required|min:3',
            'gender' => 'required',
            'phone' => 'required|unique:clients,phone',
            'address' => 'required',
            ]);
        }

        $this->ClientModel->update_client(request('full_name'), request('gender'), request('phone'), request('address'), new \DateTime(), new \DateTime(), $id);


        Session::flash('success', 'Ваша запись сохранена');

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
