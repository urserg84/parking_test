<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Car;

class CarController extends Controller
{


    public function __construct()
    {   
        $this->CarModel = new Car();
    }
     

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->CarModel->get_clients_off_parking();
        $cars = $this->CarModel->get_cars_off_parking();

        $clients_unique = [];
        foreach ($clients as $client) {
            $clients_unique[$client->id] = $client->full_name;
        }

        return view('car.index')
                ->with('cars', $cars)
                ->with('clients', $clients)
                ->with('clients_unique', $clients_unique);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'brand' => 'required',
            'model' => 'required',
            'color' => 'required',
            'number' => 'required|unique:cars,number',
            'client_id' => 'required',
            'on_parking' => 'nullable',
        ]);

        $on_parking = request('on_parking');
        if (!request('on_parking')) {
            $on_parking = 0;
        }

        $this->CarModel->store_car(request('brand'), request('model'), request('color'), request('number'), request('client_id'), $on_parking, new \DateTime(), new \DateTime());


        Session::flash('success', 'Ваша запись сохранена');

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $current_number = $this->CarModel->get_current_number($id);
        if (request('number') == $current_number) {
            request()->validate([
                'brand' => 'required',
                'model' => 'required',
                'color' => 'required',
                'number' => 'required',
                'on_parking' => 'nullable',
            ]);
        } else {
            request()->validate([
                'brand' => 'required',
                'model' => 'required',
                'color' => 'required',
                'number' => 'required|unique:cars,number',
                'on_parking' => 'nullable',
            ]);
        }

        $on_parking = request('on_parking');
        if (!request('on_parking')) {
            $on_parking = 0;
        }

        $this->CarModel->update_car(request('brand'), request('model'), request('color'), request('number'), $on_parking,  new \DateTime(), new \DateTime(), $id);


        Session::flash('success', 'Ваша запись сохранена');

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->CarModel->car_destroy($id);
        Session::flash('success', 'Машина удалена со стоянки');

        return redirect()->route('home');
    }

    public function add_to_parking()
    {
        request()->validate([
            'cars' => 'required',
        ]);
        $id = request('cars');
        $this->CarModel->add_to_parking($id);
        Session::flash('success', 'Машина добавлена на стоянку');

        return redirect()->route('home');
    }
}
