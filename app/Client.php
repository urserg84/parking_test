<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    public function get_cars()
    {
        return DB::table('cars')
                ->join('clients', 'cars.client_id', '=', 'clients.id')
                ->orderBy('cars.client_id')
                ->where('cars.on_parking', '=', '1')
                ->paginate(5);
    }

    public function store_client($full_name, 
    								$gender, 
    								$phone, 
    								$address, 
    								$created_at, 
    								$updated_at)
    {
    	DB::insert('insert into clients (full_name, gender, phone, address, created_at, updated_at) values (?, ?, ?, ?, ?, ?)', [$full_name, 
    									$gender, 
    									$phone, 
    									$address, 
    									$created_at, 
    									$updated_at]);
    }

    public function get_last_client_id()
    {
    	return DB::select('
	            select id from clients 
	            order by id DESC
	            limit 1
	        ')[0]->id;
    }

    public function store_car($brand,    
							    $model, 
							    $color, 
							    $number, 
							    $client_id, 
							    $on_parking, 
							    $created_at, 
							    $updated_at)
    {
    	DB::insert('insert into cars (brand, model, color, number, client_id, on_parking, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?)', [
    							$brand,    
							    $model, 
							    $color, 
							    $number, 
							    $client_id, 
							    $on_parking, 
							    $created_at, 
							    $updated_at
							]);
    }

    public function edit_client($id)
    {
    	return DB::select(DB::raw('
	            select * from clients cl
	            inner join cars cr
	            on cl.id = cr.client_id
	            where cl.id = ?'
	        ), [$id]);
    }

    public function get_current_phone($id)
    {
    	return DB::select('select phone from clients where id = ?', [$id])[0]->phone;;
    }

    public function update_client($full_name, 
    								$gender, 
    								$phone, 
    								$address, 
    								$created_at, 
    								$updated_at,
    								$id)
    {
    	DB::update('update clients 
            SET full_name = ?, 
            gender = ?, 
            phone = ?, 
            address = ?, 
            created_at = ?, 
            updated_at = ? 
            where id = ?', [$full_name, 
    						$gender, 
    						$phone, 
    						$address, 
    						$created_at, 
    						$updated_at,
    						$id]);
    }
}
