@extends('main')

@section('title')
 Выберите действие
@endsection

@section('content')
<div class="container d-flex justify-content-center align-items-center flex-wrap h-100">
  <div>
    <a href="{{route('home')}}" class="btn btn-outline-primary btn-sm d-block w-100 ">Просмотр атомобилей на стоянке </a>
    <a href="{{route('client.create')}}" class="btn btn-outline-primary btn-sm d-block w-100 mt-5"> Добавить клиента </a>
    <a href="{{route('car.index')}}" class="btn btn-outline-primary btn-sm d-block w-100 mt-5"> Добавить автомобиль на стоянку </a>
  </div>
</div>
@endsection
