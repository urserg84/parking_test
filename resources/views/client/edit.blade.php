@extends('main')

@section('title')
 Редактировать клиента
@endsection

@section('content')
<div class="container mt-4">
    <h3>Клиент</h3>
    <form action="{{route('client.update', $client[0]->id )}}" class="form" method="post">
    <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <div class="form-group">
                        <label for="full_name">ФИО:</label>
                        <input type="text" class="form-control" name="full_name" value="{{$client[0]->full_name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="gender">Пол:</label>
                        <select  name="gender">
                            <option value="male" @if ($client[0]->gender === 'male')
                                selected @endif >Мужской</option>
                            <option value="female" @if ($client[0]->gender === 'female')
                                selected @endif >Женский</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон:</label>
                        <input type="text" class="form-control phone-number" name="phone" value="{{$client[0]->phone}}" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Адрес:</label>
                        <input type="text" class="form-control" name="address" value="{{$client[0]->address}}" required>
                    </div>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </form>
        <h3>Автомобили:</h3>
        @foreach ($client as $car)
            <form action="{{route('car.update', $car->car_id )}}" method="post">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <div class="col-9">
                        <input name="_method" type="hidden" value="PUT">
                            <div class="form-group">
                                <label for="brand">Марка:</label>
                                <input type="text" class="form-control brandInputClass" name="brand" value="{{$car->brand}}" required >
                            </div>
                            <div class="form-group">
                                <label for="model">Модель:</label>
                                <input type="text" class="form-control" name="model" value="{{$car->model}}" required >
                            </div>
                            <div class="form-group">
                                <label for="color">Цвет:</label>
                                <input type="text" class="form-control" name="color" value="{{$car->color}}" required >
                            </div>
                            <div class="form-group">
                                <label for="number">Номер:</label>
                                <input type="text" class="form-control" name="number" value="{{$car->number}}" required>
                            </div>
                            <div class="form-group on_parking_checkbox">
                                <label for="on_parking">На стоянке:</label>
                                <input type="checkbox" class="form-control" name="on_parking" value="1" @if ($car->on_parking == 1)
                                checked @endif>
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
        @endforeach
        <h3>Добавить автомобиль:</h3>
        <form action="{{route('car.store')}}" class="form" method="post">
        <input name="client_id" type="hidden" value="{{$client[0]->id}}">
        {{ csrf_field() }}
        <div class="form-group">
			<label for="brand">Марка:</label>
			<input type="text" id="brandInput" class="form-control" name="brand" required >
		</div>
        <div class="form-group">
			<label for="model">Модель:</label>
			<input type="text" class="form-control" name="model" required >
		</div>
        <div class="form-group">
			<label for="color">Цвет:</label>
			<input type="text" class="form-control" name="color" required >
		</div>
        <div class="form-group">
			<label for="number">Номер:</label>
			<input type="text" class="form-control" name="number" required >
		</div>
        <div class="form-group on_parking_checkbox">
			<label for="nuon_parkingmber">на стоянке:</label>
			<input type="checkbox" class="form-control" name="on_parking" value="1">
		</div>
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
    
</div>
@endsection

@section('script')
<script src="/js/jquery.inputmask.bundle.js" ></script>
<script src="/js/phone.js" ></script>
<script src="/js/phone-ru.js" ></script>
<script src="/js/autocomplite.js" ></script>

<script type="text/javascript">
    $(document).ready(function(){
      $('.phone-number').inputmask({"mask": "9 (999) 999-9999"}); //specifying options
    });

    <?php 

    $brands = [];

    foreach ($carbrands as $carbrand) { 
        $brands[] = ucfirst($carbrand->brand);
    } 

    $brands = json_encode($brands);

    echo "var brands = ". $brands . ";\n";
    ?>

    autocomplete(document.getElementById("brandInput"), brands);

    var brandsInputs = document.querySelectorAll(".brandInputClass");
    for (let i = 0; i < brandsInputs.length; i++) {
        autocomplete(brandsInputs[i], brands);
    }

</script>
@endsection
