<!doctype html>
<html lang="ru">
	<head>
		@include('partials._head')
	</head>

  <body>  
  	@yield('top_bar')
		<div class="container h-100">
		  @include('partials._messages')
			@include('partials._error')
		  @yield('content')
		</div>

		  @include('partials._js')
      @yield('script')
  </body>
</html>