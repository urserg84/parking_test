@extends('main')

@section('title')
 Добавить клиента
@endsection

@section('content')
<div class="container">
    <h3>Клиент</h3>
    <form action="{{route('client.store')}}" class="form" method="post">
        {{ csrf_field() }}
        <div class="form-group">
			<label for="full_name">ФИО:</label>
			<input type="text" class="form-control" name="full_name" required>
		</div>
        <div class="form-group">
			<label for="gender">Пол:</label>
			<select  name="gender">
				<option value="male">Мужской</option>
                <option value="female">Женский</option>
			</select>
		</div>
        <div class="form-group">
			<label for="phone">Телефон:</label>
			<input type="text" class="form-control phone-number" name="phone" required >
		</div>
        <div class="form-group">
			<label for="address">Адрес:</label>
			<input type="text" class="form-control" name="address" required >
		</div>
        <h3>Основной автомобиль:</h3>
        <div class="form-group">
			<label for="brand">Марка:</label>
			<input type="text" id="brandInput" class="form-control" name="brand" required >
		</div>
        <div class="form-group">
			<label for="model">Модель:</label>
			<input type="text" class="form-control" name="model" required >
		</div>
        <div class="form-group">
			<label for="color">Цвет:</label>
			<input type="text" class="form-control" name="color" required >
		</div>
        <div class="form-group">
			<label for="number">Номер:</label>
			<input type="text" class="form-control" name="number" required >
		</div>
        <div class="form-group on_parking_checkbox">
			<label for="nuon_parkingmber">на стоянке:</label>
			<input type="checkbox" class="form-control" name="on_parking" value="1">
		</div>
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
</div>
@endsection

@section('script')
<script src="/js/jquery.inputmask.bundle.js" ></script>
<script src="/js/phone.js" ></script>
<script src="/js/phone-ru.js" ></script>
<script src="/js/autocomplite.js" ></script>

<script type="text/javascript">
	$(document).ready(function(){
	  $('.phone-number').inputmask({"mask": "9 (999) 999-9999"}); //specifying options
	});


	<?php 

	$brands = [];

	foreach ($carbrands as $carbrand) { 
		$brands[] = ucfirst($carbrand->brand);
	} 

	$brands = json_encode($brands);

	echo "var brands = ". $brands . ";\n";
	?>

    autocomplete(document.getElementById("brandInput"), brands);
</script>
@endsection
