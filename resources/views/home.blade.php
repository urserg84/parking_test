@extends('main')

@section('title')
 Доска автостоянки
@endsection

@section('top_bar')
<div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <!-- Left Side Of Navbar -->
                  <ul class="navbar-nav mr-auto">

                  </ul>

                  <!-- Right Side Of Navbar -->
                  <ul class="navbar-nav ml-auto">
                      <!-- Authentication Links -->
                      @guest
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @else
                          <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  {{ Auth::user()->name }} <span class="caret"></span>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </div>
                          </li>
                      @endguest
                  </ul>
              </div>
            </div>
        </nav>
@endsection

@section('content')
<div class="container">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ФИО</th>
      <th scope="col">Авто</th>
      <th scope="col">Номер</th>
      <th scope="col">Действия</th>
    </tr>
  </thead>
  <tbody>
    @foreach($cars as $car)
      <tr>
        <th>{{$car->full_name}}</th>
        <td>{{$car->brand}} {{$car->model}}</td>
        <td>{{$car->number}}</td>  
        <td><a href="{{route('client.edit', $car->client_id )}}" class="btn btn-outline-primary btn-sm"> Редактировать</a>
              <form action="{{route('car.destroy', $car->car_id )}}" method="post" class="d-inline">
              <input name="_method" type="hidden" value="DELETE">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-outline-danger btn-sm"> Удалить со стоянки </a>
              </form>
							</td>
      </tr>
    @endforeach
  </tbody>
</table>
<div class="text-center">
  {{$cars->links()}}
</div>
<a href="{{route('client.create')}}" class="btn btn-outline-primary btn-sm"> Добавить клиента </a>
<a href="{{route('car.index')}}" class="btn btn-outline-primary btn-sm"> Добавить автомобиль на стоянку </a>
</div>
@endsection
