@extends('main')

@section('title')
 Добавить машину на стоянку
@endsection

@section('content')
<div class="container mt-5">
    <form action="{{route('add_to_parking')}}" class="form" method="post">
    <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-5 d-flex justify-content-center align-items-center">
                    <div class="form-group">
                        <label for="clients">Клиенты:</label>
                        <select  name="clients" class="clientsselect">
							@foreach($clients_unique as $key=>$value)
							    <option value="{{$key}}">{{$value}}</option>
							@endforeach
						</select>
                    </div>
                </div>
                <div class="col-5">
                    <div class="form-group">
                        <label for="cars">Автомобили:</label>
                        <select  name="cars" class="carsselect">
							@foreach($cars as $car)
							    <option value="{{$car->car_id}}" data-client_id="{{$car->client_id}}">{{$car->brand}} {{$car->model}}</option>
							@endforeach
						</select>
                    </div>
                </div>
                <div class="col-2 d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">Добавить на стоянку</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
    $('.clientsselect').on('change', function() {
        var x = 0;
        $('.carsselect option').each(function() {
            $(this).hide();
             if ($('.clientsselect option:selected').val() == $(this).data('client_id')){
                $(this).show();
                if (x === 0) {
                    $(this).attr("selected","selected");
                    x++;
                }
             } else {
                 $(this).removeAttr("selected");
             }
        });
    });
    $( ".clientsselect" ).trigger( "change" );
</script>
@endsection

